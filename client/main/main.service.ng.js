'use strict';

angular.module('resumeApp')
.factory('$resume', function ($http) {
    return {
        get: function () {
            return $http.get('resume.json');
        }
    };
});