'use strict'

angular.module('resumeApp')
.controller('MainCtrl', function($scope, $resume) {
    $resume.get().then(function (data) {
        $scope.name = data.data.personalDetail.name;
        $scope.jobtitle = data.data.jobDetail.jobTitle;
        $scope.email = data.data.personalDetail.email;
        $scope.mobile = data.data.personalDetail.phone;
        $scope.careerObjectives1 = data.data.jobDetail.careerObjectives1;
        $scope.careerObjectives2 = data.data.jobDetail.careerObjectives2;
        $scope.workExps = data.data.jobDetail.workExps;
        $scope.keySkills = data.data.keySkills;
        $scope.educations = data.data.educations;
    });
});